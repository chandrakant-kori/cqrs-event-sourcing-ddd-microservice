package tech.evelynn.banking.cmd.api.dto;

import tech.evelynn.banking.core.dto.BasicResponse;

public class OpenAccountResponse extends BasicResponse {
    private String id;

    public OpenAccountResponse(String id, String message) {
        super(message);
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

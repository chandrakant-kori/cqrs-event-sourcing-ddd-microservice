package tech.evelynn.banking.cmd.api.controllers;

import tech.evelynn.banking.cmd.api.commands.DepositFundsCommand;
import tech.evelynn.banking.core.dto.BasicResponse;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/api/v1/depositFunds")
public class DepositFundsController {
    private final CommandGateway commandGateway;

    @Autowired
    public DepositFundsController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PutMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
    public ResponseEntity<BasicResponse> depositFunds(@PathVariable(value = "id") String id,
                                                      @Valid @RequestBody DepositFundsCommand command) {
        try {
            command.setId(id);
            commandGateway.send(command);

            return new ResponseEntity<>(new BasicResponse("Funds successfully deposited!"), HttpStatus.OK);
        } catch (Exception e) {
            String safeErrorMessage = "Error while processing request to deposit funds into bank account for id - " + id;
            System.out.println(e.toString());

            return new ResponseEntity<>(new BasicResponse(safeErrorMessage), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

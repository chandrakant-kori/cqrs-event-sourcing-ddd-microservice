package tech.evelynn.banking.core.models;

public enum AccountType {
    SAVINGS, CURRENT
}

package tech.evelynn.banking.core.events;

import java.util.Date;

import lombok.Builder;
import lombok.Data;
import tech.evelynn.banking.core.models.AccountType;

@Data
@Builder
public class AccountOpenedEvent {
    private String id;
    private String accountHolderId;
    private AccountType accountType;
    private Date creationDate;
    private double openingBalance;
}

package tech.evelynn.banking.query.api.handlers;

import tech.evelynn.banking.core.models.Account;
import tech.evelynn.banking.query.api.dto.AccountLookupResponse;
import tech.evelynn.banking.query.api.dto.EqualityType;
import tech.evelynn.banking.query.api.queries.FindAccountByHolderIdQuery;
import tech.evelynn.banking.query.api.queries.FindAccountByIdQuery;
import tech.evelynn.banking.query.api.queries.FindAccountsWithBalanceQuery;
import tech.evelynn.banking.query.api.queries.FindAllAccountsQuery;
import tech.evelynn.banking.query.api.repositories.AccountRepository;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AccountQueryHandlerImpl implements AccountQueryHandler {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountQueryHandlerImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @QueryHandler
    @Override
    public AccountLookupResponse findAccountById(FindAccountByIdQuery query) {
        Optional<Account> bankAccount = accountRepository.findById(query.getId());
        AccountLookupResponse response = bankAccount.isPresent()
                ? new AccountLookupResponse("Bank Account Successfully Returned!", bankAccount.get())
                : new AccountLookupResponse("No Bank Account Found for ID - " + query.getId());

        return response;
    }

    @QueryHandler
    @Override
    public AccountLookupResponse findAccountByHolderId(FindAccountByHolderIdQuery query) {
        Optional<Account> bankAccount = accountRepository.findByAccountHolderId(query.getAccountHolderId());
        AccountLookupResponse response = bankAccount.isPresent()
                ? new AccountLookupResponse("Bank Account Successfully Returned!", bankAccount.get())
                : new AccountLookupResponse("No Bank Account Found for Holder ID - " + query.getAccountHolderId());

        return response;
    }

    @QueryHandler
    @Override
    public AccountLookupResponse findAllAccounts(FindAllAccountsQuery query) {
        Iterable<Account> bankAccountIterator = accountRepository.findAll();

        if (!bankAccountIterator.iterator().hasNext())
            return new AccountLookupResponse("No Bank Accounts were Found!");

        List<Account> bankAccounts = new ArrayList<Account>();
        bankAccountIterator.forEach(i -> bankAccounts.add(i));
        int count = bankAccounts.size();

        return new AccountLookupResponse("Successfully Returned " + count + " Bank Account(s)!", bankAccounts);
    }

    @QueryHandler
    @Override
    public AccountLookupResponse findAccountsWithBalance(FindAccountsWithBalanceQuery query) {
        List<Account> bankAccounts = query.getEqualityType() == EqualityType.GREATER_THAN
                ? accountRepository.findByBalanceGreaterThan(query.getBalance())
                : accountRepository.findByBalanceLessThan(query.getBalance());

        AccountLookupResponse response = bankAccounts != null && bankAccounts.size() > 0
                ? new AccountLookupResponse("Successfully Returned " + bankAccounts.size() + " Bank Account(s)!", bankAccounts)
                : new AccountLookupResponse("No Bank Accounts were Found!");

        return response;
    }
}

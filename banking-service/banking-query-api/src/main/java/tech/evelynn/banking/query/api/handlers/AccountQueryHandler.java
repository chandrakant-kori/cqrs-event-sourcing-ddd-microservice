package tech.evelynn.banking.query.api.handlers;

import tech.evelynn.banking.query.api.dto.AccountLookupResponse;
import tech.evelynn.banking.query.api.queries.FindAccountByHolderIdQuery;
import tech.evelynn.banking.query.api.queries.FindAccountByIdQuery;
import tech.evelynn.banking.query.api.queries.FindAccountsWithBalanceQuery;
import tech.evelynn.banking.query.api.queries.FindAllAccountsQuery;

public interface AccountQueryHandler {
    AccountLookupResponse findAccountById(FindAccountByIdQuery query);

    AccountLookupResponse findAccountByHolderId(FindAccountByHolderIdQuery query);

    AccountLookupResponse findAllAccounts(FindAllAccountsQuery query);

    AccountLookupResponse findAccountsWithBalance(FindAccountsWithBalanceQuery query);
}

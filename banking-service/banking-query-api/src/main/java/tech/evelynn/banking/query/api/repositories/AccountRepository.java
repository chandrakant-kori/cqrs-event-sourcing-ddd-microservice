package tech.evelynn.banking.query.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import tech.evelynn.banking.core.models.Account;

public interface AccountRepository extends MongoRepository<Account, String> {

    Optional<Account> findByAccountHolderId(String accountHolderId);

    List<Account> findByBalanceGreaterThan(double balance);

    List<Account> findByBalanceLessThan(double balance);
}

package tech.evelynn.banking.query.api.handlers;

import tech.evelynn.banking.core.events.AccountClosedEvent;
import tech.evelynn.banking.core.events.AccountOpenedEvent;
import tech.evelynn.banking.core.events.FundsDepositedEvent;
import tech.evelynn.banking.core.events.FundsWithdrawnEvent;
import tech.evelynn.banking.core.models.Account;
import tech.evelynn.banking.query.api.repositories.AccountRepository;

import java.util.Optional;

import org.axonframework.config.ProcessingGroup;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@ProcessingGroup("bankaccount-group")
public class AccountEventHandlerImpl implements AccountEventHandler {
    private final AccountRepository accountRepository;

    @Autowired
    public AccountEventHandlerImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @EventHandler
    @Override
    public void on(AccountOpenedEvent event) {
        Account bankAccount = Account.builder().id(event.getId())
                .accountHolderId(event.getAccountHolderId())
                .creationDate(event.getCreationDate())
                .accountType(event.getAccountType())
                .balance(event.getOpeningBalance())
                .build();
        accountRepository.save(bankAccount);
    }

    @EventHandler
    @Override
    public void on(FundsDepositedEvent event) {
        Optional<Account> bankAccount = accountRepository.findById(event.getId());

        if (bankAccount.isPresent()) {
            bankAccount.get().setBalance(event.getBalance());
            accountRepository.save(bankAccount.get());
        }
    }

    @EventHandler
    @Override
    public void on(FundsWithdrawnEvent event) {
        Optional<Account> bankAccount = accountRepository.findById(event.getId());

        if (bankAccount.isPresent()) {
            bankAccount.get().setBalance(event.getBalance());
            accountRepository.save(bankAccount.get());
        }
    }

    @EventHandler
    @Override
    public void on(AccountClosedEvent event) {
        accountRepository.deleteById(event.getId());
    }
}

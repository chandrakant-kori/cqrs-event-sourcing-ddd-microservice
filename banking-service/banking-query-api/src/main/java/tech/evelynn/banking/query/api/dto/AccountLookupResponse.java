package tech.evelynn.banking.query.api.dto;

import tech.evelynn.banking.core.dto.BasicResponse;
import tech.evelynn.banking.core.models.Account;

import java.util.ArrayList;
import java.util.List;

public class AccountLookupResponse extends BasicResponse {

    private List<Account> accounts;

    public AccountLookupResponse(String message) {
        super(message);
    }

    public AccountLookupResponse(String message, List<Account> accounts) {
        super(message);
        this.accounts = accounts;
    }

    public AccountLookupResponse(String message, Account account) {
        super(message);
        this.accounts = new ArrayList<>();
        this.accounts.add(account);
    }

    public List<Account> getAccounts() {
        return this.accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }
}

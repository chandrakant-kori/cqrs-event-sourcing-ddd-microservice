package tech.evelynn.banking.query.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import tech.evelynn.banking.core.configuration.AxonConfig;

@SpringBootApplication
@EntityScan(basePackages = "tech.evelynn.banking.core.models")
@Import(AxonConfig.class)
public class BankingQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BankingQueryApplication.class, args);
    }

}

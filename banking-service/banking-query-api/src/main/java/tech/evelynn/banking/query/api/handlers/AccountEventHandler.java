package tech.evelynn.banking.query.api.handlers;

import tech.evelynn.banking.core.events.AccountClosedEvent;
import tech.evelynn.banking.core.events.AccountOpenedEvent;
import tech.evelynn.banking.core.events.FundsDepositedEvent;
import tech.evelynn.banking.core.events.FundsWithdrawnEvent;

public interface AccountEventHandler {
    void on(AccountOpenedEvent event);

    void on(FundsDepositedEvent event);

    void on(FundsWithdrawnEvent event);

    void on(AccountClosedEvent event);
}

package tech.evelynn.user.cmd.api.dto;

import tech.evelynn.user.core.dto.BasicResponse;

public class RegisterUserResponse extends BasicResponse {
    private String id;

    public RegisterUserResponse(String id, String message) {
        super(message);
        this.id = id;
    }
}

package tech.evelynn.user.cmd.api.aggregates;

import java.util.UUID;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

import tech.evelynn.user.cmd.api.commands.RegisterUserCommand;
import tech.evelynn.user.cmd.api.commands.RemoveUserCommand;
import tech.evelynn.user.cmd.api.commands.UpdateUserCommand;
import tech.evelynn.user.cmd.api.security.PasswordEncoder;
import tech.evelynn.user.cmd.api.security.PasswordEncoderImpl;
import tech.evelynn.user.core.events.UserRegisteredEvent;
import tech.evelynn.user.core.events.UserRemovedEvent;
import tech.evelynn.user.core.events.UserUpdatedEvent;
import tech.evelynn.user.core.models.User;

@Aggregate
public class UserAggregate {

    private final PasswordEncoder passwordEncoder;
    @AggregateIdentifier
    private String id;
    private User user;

    public UserAggregate() {
        passwordEncoder = new PasswordEncoderImpl();
    }

    @CommandHandler
    public UserAggregate(RegisterUserCommand command) {
        User newUser = command.getUser();
        newUser.setId(command.getId());
        String password = newUser.getAccount().getPassword();
        passwordEncoder = new PasswordEncoderImpl();
        String hashedPassword = passwordEncoder.hashPassword(password);
        newUser.getAccount().setPassword(hashedPassword);

        UserRegisteredEvent event = UserRegisteredEvent.builder().id(command.getId()).user(newUser).build();

        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(UpdateUserCommand command) {
        User updatedUser = command.getUser();
        updatedUser.setId(command.getId());
        String password = updatedUser.getAccount().getPassword();
        String hashedPassword = passwordEncoder.hashPassword(password);
        updatedUser.getAccount().setPassword(hashedPassword);

        UserUpdatedEvent event = UserUpdatedEvent.builder().id(UUID.randomUUID().toString()).user(updatedUser).build();

        AggregateLifecycle.apply(event);
    }

    @CommandHandler
    public void handle(RemoveUserCommand command) {
        UserRemovedEvent event = new UserRemovedEvent();
        event.setId(command.getId());

        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(UserRegisteredEvent event) {
        this.id = event.getId();
        this.user = event.getUser();
    }

    @EventSourcingHandler
    public void on(UserUpdatedEvent event) {
        this.user = event.getUser();
    }

    @EventSourcingHandler
    public void on(UserRemovedEvent event) {
        AggregateLifecycle.markDeleted();
    }
}

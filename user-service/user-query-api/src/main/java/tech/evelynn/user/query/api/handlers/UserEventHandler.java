package tech.evelynn.user.query.api.handlers;

import tech.evelynn.user.core.events.UserRegisteredEvent;
import tech.evelynn.user.core.events.UserRemovedEvent;
import tech.evelynn.user.core.events.UserUpdatedEvent;

public interface UserEventHandler {
    void on(UserRegisteredEvent event);

    void on(UserUpdatedEvent event);

    void on(UserRemovedEvent event);
}

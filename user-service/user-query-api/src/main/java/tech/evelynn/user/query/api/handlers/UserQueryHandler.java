package tech.evelynn.user.query.api.handlers;

import tech.evelynn.user.query.api.dto.UserLookupResponse;
import tech.evelynn.user.query.api.queries.FindAllUsersQuery;
import tech.evelynn.user.query.api.queries.FindUserByIdQuery;
import tech.evelynn.user.query.api.queries.SearchUsersQuery;

public interface UserQueryHandler {
    UserLookupResponse getUserById(FindUserByIdQuery query);

    UserLookupResponse searchUsers(SearchUsersQuery query);

    UserLookupResponse getAllUsers(FindAllUsersQuery query);
}

package tech.evelynn.user.oauth2.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import tech.evelynn.user.core.models.Account;
import tech.evelynn.user.core.models.User;
import tech.evelynn.user.oauth2.repositories.UserRepository;

@Service(value = "userService")
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(username);

        if (user.isPresent()) {
            Account account = user.get().getAccount();
            return org.springframework.security.core.userdetails.User
                    .withUsername(username)
                    .password(account.getPassword())
                    .authorities(account.getRoles())
                    .accountExpired(false)
                    .accountLocked(false)
                    .credentialsExpired(false)
                    .disabled(false)
                    .build();
        } else {
            throw new UsernameNotFoundException("Incorrect Username / Password supplied!");
        }

    }
}

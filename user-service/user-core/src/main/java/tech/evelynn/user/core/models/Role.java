package tech.evelynn.user.core.models;

import org.springframework.data.annotation.Version;
import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    READ_PRIVILEGE, WRITE_PRIVILEGE;

    @Version
    public String getAuthority() {
        return name();
    }
}

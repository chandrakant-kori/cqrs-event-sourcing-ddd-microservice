package tech.evelynn.apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class APIGatewayApplication {
    //Testing for the git diff
    public static void main(String[] args) {
        SpringApplication.run(APIGatewayApplication.class, args);
    }

}
